package com.adamkorzeniak.crypto.api.rest;

import com.adamkorzeniak.crypto.domain.dto.CryptoAssetDetailsDto;
import com.adamkorzeniak.crypto.domain.dto.CryptoAssetDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.math.BigDecimal;
import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
interface CryptoApiMapper {

    List<CryptoAssetDto> mapRequest(List<CryptoAssetRequest> request);

    @Mapping(target = "id", ignore = true)
    CryptoAssetDto mapRequest(CryptoAssetRequest request);

    List<CryptoAssetDetailsResponse> mapDetailsResponse(List<CryptoAssetDetailsDto> cryptoAssetDetails);

    List<CryptoAssetResponse> mapResponse(List<CryptoAssetDto> cryptoAssets);

    CryptoAssetDetailsResponse mapDetailsResponse(CryptoAssetDetailsDto cryptoAssetDetails);

    CryptoAssetResponse mapResponse(CryptoAssetDto cryptoAsset);

    default CryptoAssetsDetailsResponse mapDetailsResponse(List<CryptoAssetDetailsDto> cryptoAssetDetails, String currency) {
        return new CryptoAssetsDetailsResponse(currency, mapTotalValue(cryptoAssetDetails), mapDetailsResponse(cryptoAssetDetails));
    }

    default BigDecimal mapTotalValue(List<CryptoAssetDetailsDto> cryptoAssetDetails) {
        return cryptoAssetDetails.stream()
                .map(CryptoAssetDetailsDto::value)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
