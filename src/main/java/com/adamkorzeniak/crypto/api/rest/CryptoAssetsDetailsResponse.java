package com.adamkorzeniak.crypto.api.rest;

import java.math.BigDecimal;
import java.util.List;

record CryptoAssetsDetailsResponse(
        String currency,
        BigDecimal totalValue,
        List<CryptoAssetDetailsResponse> assets) {

}
