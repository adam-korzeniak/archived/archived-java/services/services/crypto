package com.adamkorzeniak.crypto.infrastructure.assets;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "crypto")
class CryptoAssetEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(unique = true)
    private String externalId;

    @NotBlank
    private String name;

    @NotBlank
    private String symbol;

    private String location;

    @NotNull
    @Min(0)
    private BigDecimal amount;
}
