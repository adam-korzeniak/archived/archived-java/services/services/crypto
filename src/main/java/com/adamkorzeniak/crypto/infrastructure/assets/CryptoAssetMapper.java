package com.adamkorzeniak.crypto.infrastructure.assets;

import com.adamkorzeniak.crypto.domain.dto.CryptoAssetDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
interface CryptoAssetMapper {
    CryptoAssetDto mapToDto(CryptoAssetEntity asset);

    CryptoAssetEntity mapToEntity(CryptoAssetDto cryptoAsset);

    List<CryptoAssetDto> mapToDtos(List<CryptoAssetEntity> assets);

    List<CryptoAssetEntity> mapToEntities(List<CryptoAssetDto> cryptoAssets);

}
