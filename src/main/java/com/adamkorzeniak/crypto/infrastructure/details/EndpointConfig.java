package com.adamkorzeniak.crypto.infrastructure.details;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Valid
@Configuration
@ConfigurationProperties(prefix = "app.endpoint")
class EndpointConfig {

    @NotNull
    @Valid
    private CmcService cmc;

    @Data
    static class CmcService {
        @NotBlank
        private String url;
        @NotBlank
        private String authHeader;
        @NotBlank
        private String authHeaderValue;
        @NotBlank
        private String quotesPath;
    }
}
