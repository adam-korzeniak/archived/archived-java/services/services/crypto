package com.adamkorzeniak.crypto.infrastructure.details;

import com.adamkorzeniak.crypto.infrastructure.details.dto.CryptosDetailsDto;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Mapper
interface CmcMapper {

    default CryptosDetailsDto mapDetails(CMCCryptoResponse cmcCryptoResponse, String currency) {
        if (cmcCryptoResponse == null) {
            return new CryptosDetailsDto();
        }
        Map<String, CryptoDetails> cryptoMap = cmcCryptoResponse.getCryptos().entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> mapDetails(entry.getValue(), currency)
                ));
        return new CryptosDetailsDto(cryptoMap);
    }

    @Mapping(target = "rank", source = "cmcRank")
    @Mapping(target = "price", source = "quote", qualifiedByName = "mapPrice")
    @Mapping(target = "marketCap", source = "quote", qualifiedByName = "mapMarketCap")
    CryptoDetails mapDetails(CMCCryptoResponse.CMCCrypto cmcCryptoResponse, @Context String currency);

    default String mapPlatform(CMCCryptoResponse.Platform platform) {
        if (platform == null) {
            return null;
        }
        return String.format("%s (%s)", platform.getName(), platform.getSymbol());
    }

    @Named("mapPrice")
    default BigDecimal mapPrice(Map<String, CMCCryptoResponse.CMCCryptoQuote> quote, @Context String currency) {
        return getQuote(quote, currency)
                .map(CMCCryptoResponse.CMCCryptoQuote::getPrice)
                .orElseThrow();
    }

    @Named("mapMarketCap")
    default BigDecimal mapMarketCap(Map<String, CMCCryptoResponse.CMCCryptoQuote> quote, @Context String currency) {
        return getQuote(quote, currency)
                .map(CMCCryptoResponse.CMCCryptoQuote::getMarketCap)
                .orElseThrow();
    }

    default Optional<CMCCryptoResponse.CMCCryptoQuote> getQuote(Map<String, CMCCryptoResponse.CMCCryptoQuote> quote, String currency) {
        return Stream.of(currency, currency.toUpperCase(), currency.toLowerCase())
                .map(quote::get)
                .filter(Objects::nonNull)
                .findFirst();
    }

}
