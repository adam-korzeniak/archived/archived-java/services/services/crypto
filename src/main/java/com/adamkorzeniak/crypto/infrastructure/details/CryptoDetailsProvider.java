package com.adamkorzeniak.crypto.infrastructure.details;

import com.adamkorzeniak.crypto.infrastructure.details.dto.CryptosDetailsDto;

import java.util.Set;

public interface CryptoDetailsProvider {
    CryptosDetailsDto getCryptoDetails(Set<String> externalIds, String currency);
}
