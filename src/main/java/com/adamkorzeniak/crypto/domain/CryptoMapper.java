package com.adamkorzeniak.crypto.domain;

import com.adamkorzeniak.crypto.domain.dto.CryptoAssetDetailsDto;
import com.adamkorzeniak.crypto.domain.dto.CryptoAssetDto;
import com.adamkorzeniak.crypto.infrastructure.details.CryptoDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.math.BigDecimal;

@Mapper
interface CryptoMapper {

    @Mapping(target = "name", source = "details.name")
    @Mapping(target = "symbol", source = "details.symbol")
    @Mapping(target = "platform", source = "details.platform")
    @Mapping(target = "maxSupply", source = "details.maxSupply")
    @Mapping(target = "totalSupply", source = "details.totalSupply")
    @Mapping(target = "price", source = "details.price")
    @Mapping(target = "marketCap", source = "details.marketCap")
    @Mapping(target = "amount", source = "asset.amount")
    @Mapping(target = "location", source = "asset.location")
    @Mapping(target = "value", expression = "java(calculateValue(details, asset))")
    CryptoAssetDetailsDto mapAssetDetails(CryptoDetails details, CryptoAssetDto asset);

    default BigDecimal calculateValue(CryptoDetails details, CryptoAssetDto asset) {
        return details.price().multiply(asset.amount());
    }
}
