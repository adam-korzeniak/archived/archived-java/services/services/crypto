package com.adamkorzeniak.crypto.domain.dto;

import java.math.BigDecimal;

public record CryptoAssetDetailsDto(
        String name,
        String symbol,
        Integer rank,
        String platform,
        BigDecimal maxSupply,
        BigDecimal totalSupply,
        BigDecimal price,
        BigDecimal marketCap,
        BigDecimal amount,
        BigDecimal value,
        String location) {
}
