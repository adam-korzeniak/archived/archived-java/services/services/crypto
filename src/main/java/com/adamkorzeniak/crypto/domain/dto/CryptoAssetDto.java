package com.adamkorzeniak.crypto.domain.dto;

import java.math.BigDecimal;

public record CryptoAssetDto(
        Long id,
        String name,
        String symbol,
        String externalId,
        BigDecimal amount,
        String location) {
}
