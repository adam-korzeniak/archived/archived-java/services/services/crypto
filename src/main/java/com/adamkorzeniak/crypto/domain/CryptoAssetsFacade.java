package com.adamkorzeniak.crypto.domain;

import com.adamkorzeniak.crypto.domain.dto.CryptoAssetDetailsDto;
import com.adamkorzeniak.crypto.domain.dto.CryptoAssetDto;
import com.adamkorzeniak.crypto.infrastructure.assets.CryptoAssetService;
import com.adamkorzeniak.crypto.infrastructure.details.CryptoDetailsProvider;
import com.adamkorzeniak.crypto.infrastructure.details.dto.CryptosDetailsDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CryptoAssetsFacade {

    private final CryptoAssetService cryptoAssetService;
    private final CryptoDetailsProvider cryptoDetailsProvider;
    private final CryptoMapper cryptoMapper;

    public List<CryptoAssetDetailsDto> getCryptoAssets(String currency, String sort) {
        List<CryptoAssetDto> cryptoAssets = cryptoAssetService.getAll();
        Set<String> externalIds = cryptoAssets.stream()
                .map(CryptoAssetDto::externalId)
                .collect(Collectors.toSet());
        CryptosDetailsDto cryptosDetailsDto = cryptoDetailsProvider.getCryptoDetails(externalIds, currency);
        return cryptoAssets.stream()
                .map(asset -> cryptoMapper.mapAssetDetails(cryptosDetailsDto.get(asset.externalId()), asset))
                .sorted(getComparator(sort))
                .collect(Collectors.toList());
    }

    public CryptoAssetDto createCryptoAsset(CryptoAssetDto cryptoAsset) {
        return cryptoAssetService.create(cryptoAsset);
    }

    public List<CryptoAssetDto> createCryptoAssets(List<CryptoAssetDto> cryptoAssets) {
        return cryptoAssetService.create(cryptoAssets);
    }

    private Comparator<CryptoAssetDetailsDto> getComparator(String sort) {
        if (sort.equals("name")) {
            return Comparator.comparing(CryptoAssetDetailsDto::name);
        }
        return Comparator.comparing(CryptoAssetDetailsDto::value).reversed();
    }

}
